<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});

Route::resource('produk','ProdukController');
Route::resource('pesanan','PesananController');
Route::resource('rencana','RencanaharianController');

//rencana
Route::post('rencana/store','RencanaharianController@store');

//ajax
Route::get('/pesananajax/{id}','PesananController@pesananAjax');
Route::get('/produkajax/{id}','ProdukController@produkAjax');

