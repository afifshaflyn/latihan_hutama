<?php

namespace App\Http\Controllers;
use App\Rencanaharian;
use App\Produk;
use App\Pesanan;
use Validator;

use Illuminate\Http\Request;

class RencanaharianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rencanaharians = Rencanaharian::all();
        return view('pages.index', get_defined_vars()); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rencanaharians = Rencanaharian::all();
        $produks        = Produk::all();
        $pesanans       = Pesanan::all();
        return view('pages.create', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $rules = [];
            
            array(
                'jenis_produksi.*'    =>  'required',
                'no_pesanan.*'        =>  'required',  
                'no_rekanan.*'        =>  'required',
                'nama_rekanan.*'      =>  'required',
                'no_produk.*'         =>  'required',
                'nama_produk.*'       =>  'required',
                'satuan.*'            =>  'required',
                'spek.*'              =>  'required',
                'volume.*'            =>  'required',
                'w_kerja.*'           =>  'required',
                'tanggal_rencana.*'   =>  'required'      
            );
            $error = Validator::make($request->all(), $rules);
            if($error->fails())
            {
                return response()->json([
                    'error'     =>  $error->errors()->all() 
                ]);
            }

            $jenis_produksi    = $request->jenis_produksi;
            $no_pesanan        = $request->no_pesanan;
            $no_rekanan        = $request->no_rekanan;
            $nama_rekanan      = $request->nama_rekanan;
            $no_produk         = $request->no_produk;
            $nama_produk       = $request->nama_produk;
            $satuan            = $request->satuan;
            $spek              = $request->spek;
            $volume            = $request->volume;
            $w_kerja           = $request->w_kerja;
            $tanggal_rencana   = $request->tanggal_rencana;
            for($count = 0; $count < count($jenis_produksi); $count++)
            {
                $data = array(
                    'jenis_produksi'    => $jenis_produksi['$count'],
                    'no_pesanan'        => $no_pesanan['$count'],
                    'no_rekanan'        => $no_rekanan['$count'],
                    'nama_rekanan'      => $nama_rekanan['$count'],
                    'no_produk'         => $no_produk['$count'],
                    'nama_produk'       => $nama_produk['$count'],
                    'satuan'            => $satuan['$count'],
                    'spek'              => $spek['$count'],
                    'volume'            => $volume['$count'],
                    'w_kerja'           => $w_kerja['$count'],
                    'tanggal_rencana'   => $tanggal_rencana['$count']
                );
                $insert_data[] = $data;
            }

            Rencanaharian::insert($insert_data);
            return response()->json([
                'success'   => 'Data Added successfully.'
            ]);

        }
        
     
        // Rencanaharian::create([
            // 'jenis_produksi'    => $request['jenis_produksi'],
            // 'no_pesanan'        => $request['no_pesanan'],
            // 'no_rekanan'        => $request['no_rekanan'],
            // 'nama_rekanan'      => $request['nama_rekanan'],
            // 'no_produk'         => $request['no_produk'],
            // 'nama_produk'       => $request['nama_produk'],
            // 'satuan'            => $request['satuan'],
            // 'spek'              => $request['spek'],
            // 'volume'            => $request['volume'],
            // 'w_kerja'           => $request['w_kerja'],
            // 'tanggal_rencana'   => $request['tanggal_rencana'],
        // ]);
        // return redirect()->route('rencana.create')->with('sukses','Data berhasil di input');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
