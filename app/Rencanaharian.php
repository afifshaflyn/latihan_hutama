<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rencanaharian extends Model
{
    public $timestamps =  false;
    protected $table = 'rencana_harian';
    protected $fillable = 
    [ 
        'jenis_produksi', 
        'no_pesanan', 
        'no_rekanan', 
        'nama_rekanan', 
        'no_produk', 
        'nama_produk', 
        'satuan', 
        'spek', 
        'volume', 
        'w_kerja', 
        'tanggal_rencana'
    ];
}
