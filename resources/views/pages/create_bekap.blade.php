@extends('layouts.app')


@section('title')
 Halaman Index
@endsection

@section('content')
<div class="container-fluid">
@if(session('sukses'))
<div class="alert alert-success" role="alert" >
  Data Berhasil di Input
</div>
@endif
<div class="row justify-content-center">

<form class='form-group' action="{{route('rencana.store')}}" method="POST">
{{ csrf_field() }}
<div class=''>
<br>
  <label for="">Tanggal Rencana</label>
  <input name="tanggal_rencana" type="datetime-local">
</div>
<div class='row'>
<div class="col-md-12 col-sm-12"></div>
  <table class='table table-bordered  table-striped table-hover table-sm'>
  <thead>
    <th rowspan='2'>No.</th>
    <th rowspan='2'>P/PK/K</th>
    <th colspan='3'>kontrak/pesanan</th>
    <th colspan='3'>Uraian</th>
    <th rowspan='2'>Spek</th>
    <th colspan='2'>Rencana</th>
    <th style="text-align: center" rowspan='2' ><a href="#" class="btn btn-info addRow">+</a></th>
  <tr>
      <th>No.Kontrak</th>
      <th>No.Rekanan</th>
      <th>Nama Rekanan</th>
      <th>No.Prod</th>
      <th>Nama Produk</th>
      <th>Satuan</th>
      <th>Volume</th>
      <th>W.Kerja</th> 
  </tr>
</thead>
<tbody id="tibadi">
</tbody>
<tfoot>
  <td colspan='9'>Total</td>
  <td></td>
  <td></td>
  <td></td>
</tfoot>
</table>
</div>
<input type="submit" value="Simpan" class="btn btn-primary">
</div>
</div>
</div>
</div>
</form>


<script type="text/javascript">


 
$('.addRow').on('click',function(){
    addRow();
});

$(document).ready(function(){

function addRow(){
  var html = '<tr>';
  html +=   '<div class="form-group">';
  html +=        '<td>{{++$i}}</td>';
  html +=          '<td>';
  html +=            '<select name="jenis_produksi">';
  html +=              '<option value="">Pilih Jenis</option>';
  html +=              '<option value="p">P</option>';
  html +=              '<option value="pk">PK</option>';
  html +=              '<option value="k">K</option>';
  html +=           '</select>';
  html +=          '</td>';
  html +=      '</div>';
  html +=      '<div class="form-group dropdown">';
  html +=        '<td>';
  html +=          '<select  name="no_pesanan" id="select_pesanan">';
  html +=            '<option value="">Pilih Kontrak</option>';
  html +=            '@foreach($pesanans as $pesanan)';
  html +=            '<option id="nopes_input" type="hidden" value="{{$pesanan->no_pesanan}}">{{$pesanan->no_pesanan}}</option>';
  html +=            '@endforeach';
  html +=          '</select>';
  html +=          '<td><span><p id="norek"></p><input name="no_rekanan" type="hidden" id="norek_input"></span></td>';
  html +=          '<td><span><p id="namarekanan"></p><input name="nama_rekanan" type="hidden" id="narek_input"></span></td>';
  html +=        '</td>';
  html +=      '</div>'; 
  html +=      '<div class="form-group">';
  html +=      '<td><span><p id="noprod"></p><input name="no_produk" type="hidden" id="noprod_input"></span></td>';
  html +=        '<td>';
  html +=          '<select name="nama_produk" id="select_produk" >';
  html +=            '<option value="">Pilih Produk</option>';
  html +=            '@foreach($produks as $produk)';
  html +=            '<option value="{{$produk->nama_produk}}">{{$produk->nama_produk}}</option>';
  html +=            '@endforeach';
  html +=          '</select>';
  html +=        '</td>';
  html +=      '<td><span><p id="satuan"></p><input name="satuan" type="hidden" id="satuan_input"></span></td>';
  html +=      '</div>';

  html +=      '<div class="form-group">';
  html +=      '<td>';
  html +=        '<select name="spek" id="">';
  html +=            '<option value="">Pilih Spek</option>';
  html +=            '<option value="1">1</option>';
  html +=            '<option value="2">2</option>';
  html +=            '<option value="3">3</option>';
  html +=        '</select>';
  html +=      '</td>';
  html +=          '<td><input name="volume" value="" type="text"></td>';
  html +=          '<td><input name="w_kerja" value="" type="text"></td>';
  html +=          '<td style="text-align: center"><a href="#" class="btn btn-danger remove">-</a></td></tr>';
            
  $('#tibadi').append(html);


};

$( "#select_pesanan" ).change(function () {
    var iddata = $("#select_pesanan").val();
    $.ajax({
    url: '/pesananajax/'+iddata,
    type: 'get',
    dataType: 'json',
    success: function(data){
      $.each(data, function(index, value){
        $('#norek').html(value.no_rekanan);
        $('#namarekanan').html(value.nama_rekanan);
        $('#norek_input').val(value.no_rekanan);
        $('#narek_input').val(value.nama_rekanan);
      });
    }
  });
  });



$( "#select_produk" ).change(function () {
    var iddata = $("#select_produk").val();
    $.ajax({
    url: '/produkajax/'+iddata,
    type: 'get',
    dataType: 'json',
    success: function(data){
      $.each(data, function(index, value){
        $('#noprod').html(value.no_produk);
        $('#satuan').html(value.satuan);
        $('#noprod_input').val(value.no_produk);
        $('#satuan_input').val(value.satuan);
      });
    }
  });

  $('#tibadi').on('click', '.remove', function(){
              $(this).parent().parent().remove(); 
          });


});

});


</script>

@endsection